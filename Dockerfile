FROM docker:24.0.5-dind

RUN apk add --upgrade --no-cache \
  bash \
  curl \
  jq \
  restic

COPY backup.sh /usr/bin/backup.sh
COPY setup-cron.sh /usr/bin/setup-cron.sh
RUN chmod +x /usr/bin/backup.sh /usr/bin/setup-cron.sh

ENTRYPOINT [ "/usr/bin/setup-cron.sh" ]
