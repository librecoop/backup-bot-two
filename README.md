# Librecoop Backupbot II

_This Time, It's Easily Configurable_

Automatically takes backups from volumes of running Docker Swarm services and runs pre- and post commands.

This is a fork from [coop-cloud](https://git.coopcloud.tech/coop-cloud/backup-bot-two/) that uses docker compose instead of docker swarm, which is no longer backwards compatible.

## Background

There are lots of Docker volume backup systems; all of them have one or both of these limitations:
 - You need to define all the volumes to back up in the configuration system
 - Backups require services to be stopped to take consistent copies

Backupbot II tries to help, by
1. **letting you define backups using Docker labels**, so you can **easily collect your backups for use with another system** like docker-volume-backup.
2. **running pre- and post-commands** before and after backups, for example to use database tools to take a backup from a running service.

## Deployment

### With docker compose

1. Use the `docker-compose.yml` file and use it as a template.
2. Fill the required env variables: `RESTIC_HOST`, `HOSTNAME`, `RESTIC_PASSWORD` or `RESTIC_PASSWORD_FILE`
3. Choose either S3 or STFP to connect to restic:
  1. S3: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` or `AWS_SECRET_ACCESS_KEY_FILE`
  2. SFTP: `SSH_KEY_FILE`

## Configuration

Like Traefik, or `swarm-cronjob`, Backupbot II uses access to the Docker socket to read labels from running Docker containers:

```
services:
  db:
    labels:
      backupbot.backup: "true"
      backupbot.backup.pre-hook: 'mysqldump -u root -p"$(cat /run/secrets/db_root_password)" -f /tmp/dump/dump.db'
      backupbot.backup.post-hook: "rm -rf /tmp/dump/dump.db"
      backupbot.backup.path: "/tmp/dump/,/etc/foo/"
      backupbot.backup.volumes: "true"
```

- `backupbot.backup` -- set to `true` to back up this service (REQUIRED)
- `backupbot.backup.path` -- comma separated list of file paths within the service to copy (optional)
- `backupbot.backup.pre-hook` -- command to run before copying files (optional)
- `backupbot.backup.post-hook` -- command to run after copying files (optional)
- `backupbot.backup.volumes` -- set to `true` to back up the attached volumes (optional)

As in the above example, you can reference Docker Secrets, e.g. for looking up database passwords, by reading the files in `/run/secrets` directly.
