#!/bin/bash

set -e
set -o pipefail

cron_schedule="${CRON_SCHEDULE:?CRON_SCHEDULE not set}"

echo "$cron_schedule /usr/bin/backup.sh" | crontab -
crontab -l

crond -f -d8 -L /dev/stdout
