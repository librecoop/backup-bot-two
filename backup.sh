#!/bin/bash

hostname="${HOSTNAME:?HOSTNAME not set}"

# shellcheck disable=SC2153
restic_password_file="${RESTIC_PASSWORD_FILE}"

restic_host="${RESTIC_HOST:?RESTIC_HOST not set}"

backup_path="${BACKUP_DEST:?BACKUP_DEST not set}"
backup_paths=("$backup_path")

restic_tag="${RESTIC_TAG:=coop-cloud}"

# shellcheck disable=SC2153
ssh_key_file="${SSH_KEY_FILE}"
s3_key_file="${AWS_SECRET_ACCESS_KEY_FILE}"

restic_repo=
restic_extra_options=

if [ -n "$ssh_key_file" ] && [ -f "$ssh_key_file" ]; then
	restic_repo="sftp:$restic_host:/$hostname"

	# Only check server against provided SSH_HOST_KEY, if set
	if [ -n "$SSH_HOST_KEY" ]; then
		tmpfile=$(mktemp)
		echo "$SSH_HOST_KEY" >>"$tmpfile"
		echo "using host key $SSH_HOST_KEY"
		ssh_options="-o 'UserKnownHostsFile $tmpfile'"
	elif [ "$SSH_HOST_KEY_DISABLE" = "1" ]; then
		echo "disabling SSH host key checking"
		ssh_options="-o 'StrictHostKeyChecking=No'"
	else
		echo "neither SSH_HOST_KEY nor SSH_HOST_KEY_DISABLE set"
	fi
	restic_extra_options="sftp.command=ssh $ssh_options -i $ssh_key_file $restic_host -s sftp"
fi

if [ -n "$s3_key_file" ] && [ -f "$s3_key_file" ] && [ -n "$AWS_ACCESS_KEY_ID" ]; then
	AWS_SECRET_ACCESS_KEY="$(cat "${s3_key_file}")"
	export AWS_SECRET_ACCESS_KEY
	restic_repo="s3:$restic_host:/$hostname"
fi

if [ -n "$RESTIC_PASSWORD" ] && [ -n "$AWS_ACCESS_KEY_ID" ] && [ -n "$AWS_SECRET_ACCESS_KEY" ]; then
	restic_repo="s3:$restic_host/$hostname"
fi

if [ -z "$restic_repo" ]; then
	echo "you must configure either SFTP or S3 storage, see README"
	exit 1
fi

echo "restic_repo: $restic_repo"

# Pre-bake-in some default restic options
_restic() {
	if [ -z "$restic_extra_options" ]; then
		# shellcheck disable=SC2068
		restic -p "$restic_password_file" \
			--quiet -r "$restic_repo" \
			$@
	else
		# shellcheck disable=SC2068
		restic -p "$restic_password_file" \
			--quiet -r "$restic_repo" \
			-o "$restic_extra_options" \
			$@
	fi
}

post_commands=()
if [[ \ $*\  != *\ --skip-backup\ * ]]; then
	rm -rf "${backup_path}"

	for container in $(docker ps -q); do
		service=$(docker inspect "$container" --format "{{ .Name }}" | cut -c2-)
		details=$(docker inspect "$container" --format "{{ json .Config.Labels }}")

		if echo "$details" | jq -r '.["backupbot.backup"]' | grep -q 'true'; then
			pre=$(echo "$details" | jq -r '.["backupbot.backup.pre-hook"]')
			post=$(echo "$details" | jq -r '.["backupbot.backup.post-hook"]')
			path=$(echo "$details" | jq -r '.["backupbot.backup.path"]')

			echo "backing up: $service"

			if [ "$pre" != "null" ]; then
				# run the precommand
				echo "precommand: $pre"
				docker exec "$container" sh -c "$pre"
			fi

			if [ "$path" != "null" ]; then
				for p in ${path//,/ }; do
					echo "path: $path"
					# creates the parent folder, so `docker cp` has reliable behaviour no matter if $p ends with `/` or `/.`
					dir=$backup_path/$service/$(dirname "$p")
					test -d "$dir" || mkdir -p "$dir"
					docker cp -a "$container:$p" "$dir/$(basename "$p")"
				done

				if [ "$post" != "null" ]; then
					# run the postcommand
					echo "postcommand: $post"
					docker exec "$container" sh -c "$post"
				fi
			fi

			if echo "$details" | jq -r '.["backupbot.backup.volumes"]' | grep -q 'true'; then
				for volume in $(docker inspect "$container" | jq -r '.[].Mounts[].Name'); do
					# check if its an anonymous volume
					if [[ ! "$volume" =~ ^[a-f0-9]{64}$ ]]; then
						echo "volume: $volume"
						backup_paths+=("/var/lib/docker/volumes/$volume/_data")
					fi
				done

				if [ "$post" != "null" ]; then
					# append post command
					post_commands+=("docker exec $container sh -c \"$post\"")
				fi
			fi

		fi
	done

	# check if restic repo exists, initialise if not
	if [ -z "$(_restic cat config)" ] 2>/dev/null; then
		echo "initializing restic repo"
		_restic init
	fi
fi

if [[ \ $*\  != *\ --skip-upload\ * ]]; then
	echo backup paths: "${backup_paths[@]}"
	_restic backup --host "$hostname" --tag "$restic_tag" "${backup_paths[@]}"

	if [ "$REMOVE_BACKUP_VOLUME_AFTER_UPLOAD" -eq 1 ]; then
		echo "cleaning up ${backup_path}"
		rm -rf "${backup_path}"
	fi
fi

# run post commands for volumes
for post in "${post_commands[@]}"; do
	echo "postcommand: $post"
	eval "$post"
done

if [ -n "$KEEP_POLICY" ]; then
	_restic forget --prune "$KEEP_POLICY"
fi
